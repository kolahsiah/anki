load("//ts:prettier.bzl", "prettier_test")
load("//ts:eslint.bzl", "eslint_test")
load("//ts:protobuf.bzl", "protobufjs_library")
load("//ts:typescript.bzl", "typescript")
load("@rules_python//python:defs.bzl", "py_binary")
load("@py_deps//:requirements.bzl", "requirement")
load("//ts:jest.bzl", "jest_test")

protobufjs_library(
    name = "backend_proto",
    proto = "//proto:backend_proto_lib",
    visibility = ["//visibility:public"],
)

py_binary(
    name = "genfluent",
    srcs = [
        "genfluent.py",
    ],
    deps = [
        requirement("black"),
        requirement("stringcase"),
    ],
)

genrule(
    name = "fluent_gen",
    outs = ["i18n.ts"],
    cmd = "$(location genfluent) $(location //rslib/i18n:strings.json) $@",
    tools = [
        "genfluent",
        "//rslib/i18n:strings.json",
    ],
)

typescript(
    name = "lib",
    generated = [
        ":i18n.ts",
    ],
    deps = [
        ":backend_proto",
        "@npm//@fluent/bundle",
        "@npm//@types/jest",
        "@npm//@types/long",
        "@npm//intl-pluralrules",
        "@npm//protobufjs",
        "@npm//tslib",
    ],
)

# Tests
################

prettier_test()

eslint_test()

jest_test(
    deps = [
        ":lib",
    ],
)
